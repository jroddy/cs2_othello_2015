#include "player.h"


Side s, os;
int DEPTH=3;
int value[64] = {
        -99,48,-8,6,6,-8,48,-99,
        48,-8,-16,3,3,-16,-8,48,
        -8,-16,4,4,4,4,-16,-8,
        6,3,4,0,0,4,3,6,
        6,3,4,0,0,4,3,6,
        -8,-16,4,4,4,4,-16,-8,
        48,-8,-16,3,3,-16,-8,48,
        -99,48,-8,6,6,-8,48,-99,
    }; //oops these are negative

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    b = new Board();
    s = side;

	// determines side of player
	if (s == BLACK) {
		os = WHITE;
	}
	else {
		os = BLACK;
	}	


    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
	
	//vector<Move*> moves=findMoves(s,b);
	//return mostvalue(moves);  

    b->doMove(opponentsMove, os);     // Process opponent's move.
    
    Move *move = new Move(0, 0);
    Move *maxmove = new Move(0, 0);
    int maxcount = -64;
    
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)     // Iterate through possible moves.
        {
            move->setX(i);
            move->setY(j);
            if (b->checkMove(move, s))
            {
                Board *b1 = b->copy();
                b1->doMove(move, s);
                
                int mincount = 64;
                
                mincount = minmax(b1, os, s, DEPTH);
                // recurses to a depth of DEPTH
                delete b1;
                if (mincount > maxcount)    // minimax move
                {
                    maxmove->setX(i);
                    maxmove->setY(j);
                    maxcount = mincount;
                }
            }
        }
    }
    delete move;
    
    if (maxcount == -64) // No valid moves found.
    {
        return NULL;
    }
    
    b->doMove(maxmove, s);
    return maxmove;
}

Move *Player::mostvalue(vector<Move*> moves) {
	int m=-100;
	int mindex=0;
	
	for(unsigned int i=0; i<moves.size(); i++)
	{
		if(value[moves[i]->x + 8*moves[i]->y]>m)
		{
			m=value[moves[i]->x + 8*moves[i]->y];
			mindex=i;
		}
	}
	
	return moves[mindex];
}

/* Perform a step and return the minimum. */
int Player::minmax(Board *board, Side side, Side oside, int depth)
{
    //Move *tmove = new Move(0, 0);
    int mincount = 64;
    int newcount;
	vector<Move*> moves=findMoves(side, board);    
	for(unsigned int i=0;i<moves.size();i++)
	{
            if (board->checkMove(moves[i], side))
            {
                Board *tboard = board->copy();
                tboard->doMove(moves[i], side);
                if (depth == DEPTH)
                {
					if((moves[i]->x==0 or moves[i]->x==7) and (moves[i]->y==0 or moves[i]->y==7))
						return 15;
					else if((moves[i]->x==1 or moves[i]->x==6) and (moves[i]->y==1 or moves[i]->y==6))
						return -50;
					else if((moves[i]->x==0 and (moves[i]->y==1 or moves[i]->y==6)) or (moves[i]->x==1 and (moves[i]->y==0 or moves[i]->y==7)) or (moves[i]->x==6 and (moves[i]->y==0 or moves[i]->y==7)) or (moves[i]->x==7 and (moves[i]->y==1 or moves[i]->y==6)))
						return -20;
					else if(((moves[i]->y==0 or moves[i]->y==7) and (moves[i]->x!=6 or moves[i]->x!=1)) or ((moves[i]->x==0 or moves[i]->x==7) and (moves[i]->y==1 or moves[i]->y==6)))
						return 10;
					
						
				}
                if (depth == 0)
                {
                    newcount = tboard->countGain(oside);
				}
                else
                {
                    newcount = minmax(tboard, oside, side, depth - 1);
                }
                if (newcount < mincount)
				{
					mincount = newcount;	
                }
                delete tboard;
            }
    }
    return mincount;
}


/* Set a player's board. */
void Player::setBoard(Board *board) {
    b = board;
}

vector<Move*> Player::findMoves(Side side, Board *board) {
	
	std::vector<Move*> moves;
	for (int i=0;i<8;i++) {
        for (int j=0;j<8;j++) {
            Move *move=new Move(i, j);
            if (board->checkMove(move, side))
				moves.push_back(move);
        }
    }
    return moves;
}
