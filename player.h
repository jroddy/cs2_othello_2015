#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <vector>
using namespace std;

class Player {

private:
    Board *b;
    Side s, os;
public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    int minmax(Board *board, Side sd, Side osd, int left);
    void setBoard(Board *board);
	vector<Move*> findMoves(Side side, Board *board);
	Move *mostvalue(vector<Move*> moves);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;

};

#endif
